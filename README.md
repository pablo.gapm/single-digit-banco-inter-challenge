# Desafio Java Banco Inter 

## Descrição do Projeto
<p align="center">Desafio de conversão de dígito único e vinculação a um usuário no Banco Dados.</p>

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [SpringBoot](https://spring.io/projects/spring-boot)
- [Hibernate](https://hibernate.org/)
- [Swagger](https://swagger.io/)
- [Apache Maven](https://maven.apache.org/)


<h4 align="center"> 
	🚧  Single Digit - Pendente algumas funcionalidades...  🚧
</h4>

### Features

- [x] Controller Single Digit
- [x] Controller User
- [x] User Dto & SigleDigit Dto
- [ ] Entidades
- [ ] Testes
- [ ] postman_collection.json





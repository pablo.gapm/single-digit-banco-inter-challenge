package br.com.bancointer.singledigit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.com.bancointer.singledigit.entities.SingleDigit;
import br.com.bancointer.singledigit.repositories.SingleDigitRepository;
import br.com.bancointer.singledigit.service.SingleDigitService;

@Service
@CacheConfig(cacheNames = {"digitos"})

public abstract class SingleDigitServiceImpl implements SingleDigitService 
{
	/*
     * Definindo as regras de cálculo conforme especificado na documentação do desafio
     *
     */
    private static long checkLengthAndSum(long value) 
    {
      while (checkLength(value)) 
      {
        value = sum(value);
      }
      return value;
    }

    private static boolean checkLength(long value) 
    {
      return Long.toString(value).length() > 1;
    }

    private static long sum(long digit) 
    {
      long result = 0;
      while (digit != 0) {
        result += digit % 10;
        digit = digit / 10;
      }
      
      return result;
    }
    
    @Autowired
    private SingleDigitRepository singleDigitRepository;

    /*
     * Calculando o digito unico.
     */
    
    @Cacheable
    @Override
    public SingleDigit calculating (Integer number, Integer multiple) 
    {
       
        String numberForCalculate = number.toString();
        
        return new SingleDigit (number, multiple, calculateSingleDigit(numberForCalculate));
    }

    protected abstract String calculateSingleDigit(String numberForCalculate);

	/*
     * Aplicando o multiplicador conforme especificado na documentação.
     */
    
    private String multiplierApp(Integer multiplicate, String number)
    {
        StringBuilder multipledNumber = new StringBuilder();

        for(int i = 0; i < multiplicate; i++)
        {
            multipledNumber.append(number);
        }
        return multipledNumber.toString();
    }

    /*
     * Persistindo dados do digito no banco.
     */
    
    @Override
    public SingleDigit saving (SingleDigit singleDigit) 
    {
        return singleDigitRepository.save(singleDigit);
    }
}
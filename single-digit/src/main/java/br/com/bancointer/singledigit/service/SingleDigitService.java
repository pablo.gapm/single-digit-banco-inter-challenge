package br.com.bancointer.singledigit.service;

import br.com.bancointer.singledigit.entities.SingleDigit;

public interface SingleDigitService {
	
	
	/** Persistindo banco de dados */
	
	SingleDigit saving (SingleDigit singleDigit);
	
	/** Calculando o digito e fazendo a vinculação ao usuario */
	
	Long convert(String digit, String quantity);
	SingleDigit calculating (Integer number, Integer multiple);
}

package br.com.bancointer.singledigit.handler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import br.com.bancointer.singledigit.handler.exeception.Authetication.*;
import br.com.bancointer.singledigit.service.SingleDigitService;


@Component
public class Handler {
  public SingleDigitService service;
  private String authentication;

  Handler(SingleDigitService service, @Value("${api.authentication}") String authentication) {
    this.service = service;
    this.authentication = authentication;
  }

  public Long convertDigit(String digit, String quantity, String authentication) {
    checkAuthentication(authentication);
    return this.service.convert(digit, quantity);
  }

  private void checkAuthentication(String authentication) {
    if (!this.authentication.equals(authentication)) {
      throw new AuthenticationException();
    }
  }
}

package br.com.bancointer.singledigit.controller;

import br.com.bancointer.singledigit.Dto.SingleDigitDto;
import br.com.bancointer.singledigit.entities.SingleDigit;
import br.com.bancointer.singledigit.service.SingleDigitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.bancointer.singledigit.handler.Handler;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.Optional;

@RestController
@RequestMapping("/single-digit/user")

public class SingleUserController {

	private final Handler handler = null;
	
	/** Calculando o dígito único.
	 * 
	 * @param singleDigitDto
	 * @return ResponseEntity <SingleUserDto>
	 */
	/** Seguindo os padrões de boas práticas de API do Inter, referenciado
	 * no documento "API GUIDE" no Confluence de Back End Java em relação aos codigos HTTP.
	 */

	@ApiOperation(
			value = "Cadastrar um novo digito e vincular a um usuário")
	
	@ApiResponses(value = 
{
    @ApiResponse(
    		code = 200, 
    		message = "Retorna um dígito único",
    		response = SingleDigitDto.class),
    
    @ApiResponse(
    		code = 403, 
    		message = "Usuário não autorizado a acessar esse serviço."),
    
    @ApiResponse(
    		code = 500, 
    		message = "Retorna uma lista de erros"),
})
	@PostMapping
    public ResponseEntity<Response<SingleUserDto>> savingId(@Valid @RequestBody SinglerUserDto singlerUserDto) 
	{
        Response<UsuarioDto> response = new Response<>();
        return ResponseEntity.ok(response);
    }
	
	@ApiOperation(
			value = "Deletar um usuário do banco de dados")
	
	@ApiResponses(value = 
{
    @ApiResponse(
    		code = 200, 
    		message = "Usuario deletado com sucesso.",
    		response = SingleDigitDto.class),
    
    @ApiResponse(
    		code = 403, 
    		message = "Usuário não autorizado a acessar esse serviço."),
    
    @ApiResponse(
    		code = 500, 
    		message = "Retorna uma lista de erros"),
})
	
	@DeleteMapping
    public ResponseEntity<Response<SingleUserDto>> deleteId(@RequestBody SingleUserDto singlerUserDto) 
	{
        Response<SingleUserDto> response = new Response<>();
            return ResponseEntity.badRequest().body(response);
    }
	
	@ApiOperation(
			value = "Buscar um usuario por ID")
	
	@ApiResponses(value = 
{
    @ApiResponse(
    		code = 200, 
    		message = "Retorna o usuario buscado pelo o ID",
    		response = SingleDigitDto.class),
    
    @ApiResponse(
    		code = 403, 
    		message = "Usuário não autorizado a acessar esse serviço."),
    
    @ApiResponse(
    		code = 500, 
    		message = "Retorna uma lista de erros"),
})
	 @RequestMapping(value = "/{id}")
	    public ResponseEntity <Response<SingleUserDto>> searchForId(@PathVariable("id") Long id) {
	    
	        Optional<SingleUserDto> userId = singleUserService.searchForId(id);

	        if(!user.isPresent()){
	            return ResponseEntity.badRequest().body(response);
	        }

	      
	    }
}

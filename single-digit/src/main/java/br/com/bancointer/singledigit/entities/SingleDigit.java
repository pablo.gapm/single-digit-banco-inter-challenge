package br.com.bancointer.singledigit.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="singleDigitInter")

@Getter @Setter @NoArgsConstructor

public class SingleDigit implements Serializable
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Long id;

    @Column
    public Integer number;

    @Column
    public String multiplicate;

    @Column
    public Integer result;

    @NotNull
    
    /** Poderia também ser usado o ManyToOne FetchType.EAGER, mas como queremos que os dados sejam mostrados
     * apenas quando pesquisamos, para melhor desempenho utilizarei o FechType LAZY
     */
    
    @ManyToOne(fetch = FetchType.LAZY)
    public SingleUser user;

    public SingleDigit(Integer result, Integer number, String multiplicate) 
    {
        
    	this.result = result;
        
    	this.number =  number;
        
    	this.multiplicate = multiplicate;
    }
}

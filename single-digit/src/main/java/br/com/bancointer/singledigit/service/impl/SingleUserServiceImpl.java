package br.com.bancointer.singledigit.service.impl;

import br.com.bancointer.singledigit.entities.SingleUser;
import br.com.bancointer.singledigit.repositories.SingleUserRepository;
import br.com.bancointer.singledigit.service.SingleUserService;
import org.springframework.stereotype.Service;

import java.util.Optional;

public class SingleUserServiceImpl
{
	  /*
     * Buscar usuário por ID no banco de dados
     *
     */
    @Override
    public Optional<ClientUser> searchId(Long id)  
    {
        return Optional.ofNullable(SingleUserRepository.findOne(id));
    }

    /*
     * Persistindo um usuário no banco
     */
    @Override
    public Optional<ClientUser> savingId(ClientUser userId) 
    {
        return Optional.ofNullable(SingleUserRepository.save(userId));
    }

    /*
     * Deletando um usuario do banco de dados
     */
    @Override
    public Optional<ClientUser> deleteId(Long id) 
    {
        usuarioRepository.delete(id);
        return Optional.ofNullable(new ClientUser(id));
    }
}
}

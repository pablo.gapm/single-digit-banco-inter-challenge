package br.com.bancointer.singledigit.repositories;

import br.com.bancointer.singledigit.entities.SingleUser;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SingleUserRepository extends JpaRepository<SingleUser, Long>{
}

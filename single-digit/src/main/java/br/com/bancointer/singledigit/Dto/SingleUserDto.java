package br.com.bancointer.singledigit.Dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@SuppressWarnings("deprecation")

public class SingleUserDto 
{
	private Long id;

    @NotEmpty(message = "Insira um nome válido.")
    private String name;

    @NotEmpty(message = "Insira um e-mail válido.")
    @Email(message = "Email inválido.")
    
    private String email;

}


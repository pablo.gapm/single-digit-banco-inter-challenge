package br.com.bancointer.singledigit.service;

import br.com.bancointer.singledigit.entities.SingleUser;

import java.util.Optional;

public class SingleUserService 
{
	 /**
     * Retorna um id por usuário.
    */
    Optional<SingleUser> searchId(Long id) {
		return null;
	}
	
    /**
     * Cadastra um usuário no banco de dados.
     */
    
    Optional<SingleUser> savingId(SingleUser userId) 
    {
		return null;
	}
   
    /**
     * Deletar um usuário no banco de dados.
     */
    Optional<SingleUser> deleteId(Long id) {
		return null;
	}
    
    /* Como não temos uma chamada complexa iremos utilizar o metodo Optional, caso tivessemos, utilizaríamos
     * o orElse ou orElseGet.
     */

}

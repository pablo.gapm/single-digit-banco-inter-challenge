package br.com.bancointer.singledigit.Dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter 

public class SingleDigitDto 
{	
	@NotNull
    private Long id;

    @Min(1)
    private Long userId;
    
    @Min(1)
    private Integer number;
    
    @Min(1)
    private Integer multiplicate;
   
    private String result;
    
    public SingleDigitDto(Integer number, Integer multiplicate, String result, Long userId) {
        this.number = number;
        this.multiplicate = multiplicate;
        this.result = result;
        this.userId = userId;
    }
}
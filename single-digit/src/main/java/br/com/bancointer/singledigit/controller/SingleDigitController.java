package br.com.bancointer.singledigit.controller;

import br.com.bancointer.singledigit.Dto.SingleDigitDto;
import br.com.bancointer.singledigit.entities.SingleDigit;
import br.com.bancointer.singledigit.service.SingleDigitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import br.com.bancointer.singledigit.handler.Handler;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController

@RequestMapping("/single-digit")

class SingleDigitController 
{
	private final Handler handler = null;
	
	/** Calculando o dígito único.
	 * 
	 * @param singleDigitDto
	 * @return ResponseEntity <SingleDigitDto>
	 */
	/** Seguindo os padrões de boas práticas de API do Inter, referenciado
	 * no documento "API GUIDE" no Confluence de Back End Java em relação aos codigos HTTP.
	 */

	@ApiOperation(
			value = "Cadastrar um novo digito e vincular a um usuário")
	
	@ApiResponses(value = 
{
    @ApiResponse(
    		code = 200, 
    		message = "Retorna um dígito único",
    		response = SingleDigitDto.class),
    
    @ApiResponse(
    		code = 403, 
    		message = "Usuário não autorizado a acessar esse serviço."),
    
    @ApiResponse(
    		code = 500, 
    		message = "Retorna uma lista de erros"),
})
	
	@PostMapping
    public ResponseEntity <SingleDigitDto> calculating (@RequestBody SingleDigitDto singleDigitDto) 
    {
        SingleDigit singleDigit = singleDigitService.calculate(singleDigitDto.getNumber(), 
        		singleDigitDto.getMultiplicate());
        {
        return ResponseEntity.ok(this.handler.calculating(singleDigitDto));
        }
        		
    }
	
/** Vinculando o digito a um usuario */
	
public void userVinculate(SingleDigit singleDigit)
{
    userVerify(singleDigit.getUser().getId());

    if(singleDigit.getUser().getId() != null){
        singleDigitService.save(singleDigit);
    }
}

}


package br.com.bancointer.singledigit.repositories;

import br.com.bancointer.singledigit.entities.SingleDigit;
import org.springframework.data.jpa.repository.JpaRepository;


	public interface SingleDigitRepository extends JpaRepository <SingleDigit, Double>{
}

package br.com.bancointer.singledigit.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="ClientUser")
@Getter @Setter 

public class SingleUser implements Serializable
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;
    
    @NotNull
    @Column
    private String email;
}